# region imports
import pandas as pd
import numpy as np
from scipy.stats import skew
from sklearn.preprocessing import StandardScaler, LabelEncoder, Imputer
from sklearn.model_selection import train_test_split
# endregion

df_train = pd.read_csv('YOUR_PATH/input/train_house.csv')
df_pred = pd.read_csv('YOUR_PATH/input/pred_house.csv')

# region drop outliers
df_train = df_train[df_train.GrLivArea < 4000]
# endregion

df_target = df_train['SalePrice']
df_train = df_train.drop('SalePrice', 1) # ID delete dibawah, biar gk error

# region drop missing data
# delete missing features or observation
# following https://www.kaggle.com/pmarcelino/comprehensive-data-exploration-with-python
# drop 1 observation, Electrical di pred set gk ad yg null
null_index = df_train.loc[df_train['Electrical'].isnull()].index
df_train = df_train.drop(null_index)
df_target = df_target.drop(null_index)

total_train = df_train.isnull().sum().sort_values(ascending=False)
percent_train = (df_train.isnull().sum() / df_train.isnull().count()).sort_values(ascending=False)
missing_data_train = pd.concat([total_train, percent_train], axis=1, keys=['Total', 'Percent'])

# don't delete important features
missing_data_train = missing_data_train.drop('GarageCond', 0)
missing_data_train = missing_data_train.drop('GarageQual', 0)

# combine = combine.drop((missing_data[missing_data['Total'] > 1]).index, 1)
df_train = df_train.drop((missing_data_train[missing_data_train['Total'] > 1]).index, 1)
# drop columns in prediction based on missing training data
df_pred = df_pred.drop((missing_data_train[missing_data_train['Total'] > 1]).index, 1)

# untuk cek sisa missing data di pred
total_pred = df_pred.isnull().sum().sort_values(ascending=False)
percent_pred = (df_pred.isnull().sum() / df_pred.isnull().count()).sort_values(ascending=False)
missing_data_pred = pd.concat([total_pred, percent_pred], axis=1, keys=['Total', 'Percent'])
# endregion

# region generate missing data
df_train['GarageCond'].fillna('NA', inplace=True)
df_train['GarageQual'].fillna('NA', inplace=True)
df_pred['GarageCond'].fillna('NA', inplace=True)
df_pred['GarageQual'].fillna('NA', inplace=True)

df_pred['BsmtFullBath'].fillna(df_pred['BsmtFullBath'].dropna().mean(), inplace=True)
df_pred['BsmtHalfBath'].fillna(df_pred['BsmtHalfBath'].dropna().mean(), inplace=True)
df_pred['GarageCars'].fillna(df_pred['GarageCars'].dropna().mean(), inplace=True)
df_pred['GarageArea'].fillna(df_pred['GarageArea'].dropna().mean(), inplace=True)
df_pred['BsmtFinSF1'].fillna(df_pred['BsmtFinSF1'].dropna().mean(), inplace=True)
df_pred['TotalBsmtSF'].fillna(df_pred['TotalBsmtSF'].dropna().mean(), inplace=True)
df_pred['BsmtUnfSF'].fillna(df_pred['BsmtUnfSF'].dropna().mean(), inplace=True)
df_pred['BsmtFinSF2'].fillna(df_pred['BsmtFinSF2'].dropna().mean(), inplace=True)

df_pred['MSZoning'].fillna(df_pred['MSZoning'].dropna().mode()[0], inplace=True)
df_pred['Utilities'].fillna(df_pred['Utilities'].dropna().mode()[0], inplace=True)
df_pred['Functional'].fillna(df_pred['Functional'].dropna().mode()[0], inplace=True)
df_pred['KitchenQual'].fillna(df_pred['KitchenQual'].dropna().mode()[0], inplace=True)
df_pred['Exterior1st'].fillna(df_pred['Exterior1st'].dropna().mode()[0], inplace=True)
df_pred['Exterior2nd'].fillna(df_pred['Exterior2nd'].dropna().mode()[0], inplace=True)
df_pred['SaleType'].fillna(df_pred['SaleType'].dropna().mode()[0], inplace=True)
# endregion

combine = pd.concat([df_train, df_pred])

# region  convert to int & create new features
# ordinal
label_encoder = LabelEncoder()
combine['MSSubClass'] = label_encoder.fit_transform(combine['MSSubClass'].values)
combine['LandSlope'] = label_encoder.fit_transform(combine['LandSlope'].values)
combine['ExterQual'] = label_encoder.fit_transform(combine['ExterQual'].values)
combine['ExterCond'] = label_encoder.fit_transform(combine['ExterCond'].values)
combine['KitchenQual'] = label_encoder.fit_transform(combine['KitchenQual'].values)
combine['Functional'] = label_encoder.fit_transform(combine['Functional'].values)
combine['GarageQual'] = label_encoder.fit_transform(combine['GarageQual'].values)
combine['GarageCond'] = label_encoder.fit_transform(combine['GarageCond'].values)

# binary
combine['CentralAir'] = label_encoder.fit_transform(combine['CentralAir'].values)

combine["OverallGrade"] = combine["OverallQual"] * combine["OverallCond"]
combine["GarageGrade"] = combine["GarageQual"] * combine["GarageCond"]
combine["ExterGrade"] = combine["ExterQual"] * combine["ExterCond"]
combine["KitchenScore"] = combine["KitchenAbvGr"] * combine["KitchenQual"]
combine["GarageScore"] = combine["GarageArea"] * combine["GarageQual"]
combine["TotalBath"] = combine["BsmtFullBath"] + (0.5 * combine["BsmtHalfBath"]) + \
combine["FullBath"] + (0.5 * combine["HalfBath"])
combine["AllSF"] = combine["GrLivArea"] + combine["TotalBsmtSF"]
combine["AllFlrsSF"] = combine["1stFlrSF"] + combine["2ndFlrSF"]
combine["AllPorchSF"] = combine["OpenPorchSF"] + combine["EnclosedPorch"] + \
combine["3SsnPorch"] + combine["ScreenPorch"]

# Find most important features with respect to target
# corr = train.corr()
# corr.sort_values(["SalePrice"], ascending = False, inplace = True)
# print(corr.SalePrice)

# 3* Polynomials on the top 10 existing features
combine["OverallQual-s2"] = combine["OverallQual"] ** 2
combine["OverallQual-s3"] = combine["OverallQual"] ** 3
combine["OverallQual-Sq"] = np.sqrt(combine["OverallQual"])
combine["AllSF-2"] = combine["AllSF"] ** 2
combine["AllSF-3"] = combine["AllSF"] ** 3
combine["AllSF-Sq"] = np.sqrt(combine["AllSF"])
combine["AllFlrsSF-2"] = combine["AllFlrsSF"] ** 2
combine["AllFlrsSF-3"] = combine["AllFlrsSF"] ** 3
combine["AllFlrsSF-Sq"] = np.sqrt(combine["AllFlrsSF"])
combine["GrLivArea-2"] = combine["GrLivArea"] ** 2
combine["GrLivArea-3"] = combine["GrLivArea"] ** 3
combine["GrLivArea-Sq"] = np.sqrt(combine["GrLivArea"])
combine["ExterQual-2"] = combine["ExterQual"] ** 2
combine["ExterQual-3"] = combine["ExterQual"] ** 3
combine["ExterQual-Sq"] = np.sqrt(combine["ExterQual"])
combine["GarageCars-2"] = combine["GarageCars"] ** 2
combine["GarageCars-3"] = combine["GarageCars"] ** 3
combine["GarageCars-Sq"] = np.sqrt(combine["GarageCars"])
combine["TotalBath-2"] = combine["TotalBath"] ** 2
combine["TotalBath-3"] = combine["TotalBath"] ** 3
combine["TotalBath-Sq"] = np.sqrt(combine["TotalBath"])
combine["KitchenQual-2"] = combine["KitchenQual"] ** 2
combine["KitchenQual-3"] = combine["KitchenQual"] ** 3
combine["KitchenQual-Sq"] = np.sqrt(combine["KitchenQual"])
combine["GarageScore-2"] = combine["GarageScore"] ** 2
combine["GarageScore-3"] = combine["GarageScore"] ** 3
combine["GarageScore-Sq"] = np.sqrt(combine["GarageScore"])

# categorical
MSZoning = pd.get_dummies(combine['MSZoning'], prefix='MSZoning')
Street = pd.get_dummies(combine['Street'], prefix='Street')
LotShape = pd.get_dummies(combine['LotShape'], prefix='LotShape')
LandContour = pd.get_dummies(combine['LandContour'], prefix='LandContour')
Utilities = pd.get_dummies(combine['Utilities'], prefix='Utilities')
LotConfig = pd.get_dummies(combine['LotConfig'], prefix='LotConfig')
Neighborhood = pd.get_dummies(combine['Neighborhood'], prefix='Neighborhood')
Condition1 = pd.get_dummies(combine['Condition1'], prefix='Condition1')
Condition2 = pd.get_dummies(combine['Condition2'], prefix='Condition2')
BldgType = pd.get_dummies(combine['BldgType'], prefix='BldgType')
HouseStyle = pd.get_dummies(combine['HouseStyle'], prefix='HouseStyle')
RoofStyle = pd.get_dummies(combine['RoofStyle'], prefix='RoofStyle')
RoofMatl = pd.get_dummies(combine['RoofMatl'], prefix='RoofMatl')
Exterior1st = pd.get_dummies(combine['Exterior1st'], prefix='Exterior1st')
Exterior2nd = pd.get_dummies(combine['Exterior2nd'], prefix='Exterior2nd')
Foundation = pd.get_dummies(combine['Foundation'], prefix='Foundation')
Heating = pd.get_dummies(combine['Heating'], prefix='Heating')
HeatingQC = pd.get_dummies(combine['HeatingQC'], prefix='HeatingQC')
Electrical = pd.get_dummies(combine['Electrical'], prefix='Electrical')
PavedDrive = pd.get_dummies(combine['PavedDrive'], prefix='PavedDrive')
SaleType = pd.get_dummies(combine['SaleType'], prefix='SaleType')
SaleCondition = pd.get_dummies(combine['SaleCondition'], prefix='SaleCondition')

combine = pd.concat([combine, MSZoning, Street, LotShape, LandContour, Utilities, LotConfig,
                     Neighborhood, Condition1, Condition2, BldgType, HouseStyle,
                     RoofStyle, RoofMatl, Exterior1st, Exterior2nd, Foundation, Heating, HeatingQC,
                     Electrical, PavedDrive, SaleType, SaleCondition], axis=1)

combine = combine.drop(['MSZoning', 'Street', 'LotShape', 'LandContour', 'Utilities', 'LotConfig',
                     'Neighborhood', 'Condition1', 'Condition2', 'BldgType', 'HouseStyle',
                     'RoofStyle', 'RoofMatl', 'Exterior1st', 'Exterior2nd', 'Foundation', 'Heating', 'HeatingQC',
                     'Electrical', 'PavedDrive', 'SaleType', 'SaleCondition'], axis=1)

# endregion

# region Standarization & log transform skewness
numerical_features = combine.select_dtypes(include=[np.float64, np.int64]).columns.values
numerical_features = np.delete(numerical_features, 0) # remove Id

skewness = combine[numerical_features].apply(lambda x: skew(x))
skewness = skewness[abs(skewness) > 0.5]
# print(str(skewness.shape[0]) + " skewed numerical features to log transform")
skewed_features = skewness.index
combine[skewed_features] = np.log1p(combine[skewed_features])

df_train = pd.DataFrame(combine.iloc[:len(df_train)])
df_pred = pd.DataFrame(combine.iloc[len(df_train):])

stdsc = StandardScaler()
df_train[numerical_features] = stdsc.fit_transform(df_train[numerical_features])
df_pred[numerical_features] = stdsc.transform(df_pred[numerical_features])

df_train = df_train.drop('Id', 1)
# endregion  &

df_train.to_csv('YOUR_PATH/output/df_train.csv', index=False)
df_pred.to_csv('YOUR_PATH/output/df_pred.csv', index=False)
df_target.to_csv('YOUR_PATH/output/df_target.csv', index=False, header=['Target'])


