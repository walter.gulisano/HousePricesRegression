# region imports
import pandas as pd
import numpy as np
from sklearn import tree
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from pydotplus import graph_from_dot_data
# endregion

df_train = pd.read_csv('YOUR_PATH/output/df_train.csv')
df_pred = pd.read_csv('YOUR_PATH/output/df_pred.csv')
df_target = pd.read_csv('YOUR_PATH/output/df_target.csv')

X_train, X_test, y_train, y_test = train_test_split(df_train, df_target,
                                                    test_size=0.2, random_state=1)

# region filtered features by Lasso 
filtered_features = [
'RoofMatl_WdShngl',
'Neighborhood_StoneBr',
'Neighborhood_NridgHt',
'Neighborhood_NoRidge',
'GrLivArea',
'SaleType_New',
'OverallQual',
'Neighborhood_Crawfor',
'Exterior1st_BrkFace',
'Exterior2nd_ImStucc',
'Condition1_Norm',
'LotConfig_CulDSac',
'BsmtFinSF1',
'YearBuilt',
'LandContour_HLS',
'GarageCars',
'Neighborhood_Somerst',
'OverallCond',
'TotRmsAbvGrd',
'Functional',
'LotArea',
'WoodDeckSF',
'TotalBsmtSF',
'HouseStyle_SFoyer',
'HouseStyle_SLvl',
'ScreenPorch',
'BsmtFullBath',
'Fireplaces',
'Neighborhood_BrkSide',
'FullBath',
'BsmtFinSF2',
'LandSlope',
'LotShape_IR2',
'HeatingQC_Ex',
'PoolArea',
'Exterior1st_MetalSd',
'Foundation_PConc',
'HalfBath',
'1stFlrSF',
'BsmtHalfBath',
'ExterCond',
'MiscVal',
'SaleType_WD',
'HeatingQC_Gd',
'Exterior1st_Wd Sdng',
'EnclosedPorch',
'3SsnPorch',
'Neighborhood_Gilbert',
'OpenPorchSF',
'Neighborhood_NAmes',
'YrSold',
'Exterior1st_HdBoard',
'Neighborhood_OldTown',
'HouseStyle_1.5Fin',
'RoofStyle_Gable',
'LowQualFinSF',
'MoSold',
'SaleType_COD',
'BldgType_Twnhs',
'KitchenAbvGr',
'BedroomAbvGr',
'HouseStyle_2Story',
'SaleCondition_Abnorml',
'Neighborhood_Edwards',
'MSSubClass',
'ExterQual',
'Neighborhood_NWAmes',
'BldgType_TwnhsE',
'KitchenQual',
'LandContour_Bnk',
'Condition2_PosN',
'RoofMatl_ClyTile'
]
# endregion ()

# X_train = X_train[filtered_features]
# X_test = X_test[filtered_features]

parameters = {
                'criterion': ['mse', 'friedman_mse', 'mae'], # kalo pake semua features, bagus mse?
                'min_samples_leaf': [5, 10, 20, 25, 30],
                'max_depth': [6, 9, 12, 15, 20],
                'max_features': [None, 'sqrt', 'log2'],
                'presort': [True],
                'random_state': [0]
             }

model = DecisionTreeRegressor()

grid_obj = GridSearchCV(model, parameters, cv=10, scoring='r2')
grid_obj = grid_obj.fit(X_train, y_train)

print(grid_obj.best_estimator_)
print('Best GridSearchCV Score : ' + str(grid_obj.best_score_))

model = grid_obj.best_estimator_

model.fit(X_train, y_train)

y_pred_train = model.predict(X_train)
y_pred_test = model.predict(X_test)

print('Train MAE : ' + str(mean_absolute_error(y_train, y_pred_train)))
print('Train R^2 : ' + str(r2_score(y_train, y_pred_train)))

print('Test R^2 : ' + str(r2_score(y_test, y_pred_test)))
print('Test MSE : ' + str(mean_squared_error(y_test, y_pred_test)))
print('Test MAE : ' + str(mean_absolute_error(y_test, y_pred_test)))

# print decision tree
dot_data = tree.export_graphviz(model,
                                filled=True,
                                rounded=True,
                                class_names=['0', '1'],
                                feature_names=X_train.columns.values,
                                out_file=None)

graph = graph_from_dot_data(dot_data)
graph.write_png('/home/tama/PycharmProjects/KaggleHouse/output/tree.png')