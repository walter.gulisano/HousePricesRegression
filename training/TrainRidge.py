import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import RidgeCV
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error

df_train = pd.read_csv('YOUR_PATH/output/df_train.csv')
df_pred = pd.read_csv('YOUR_PATH/output/df_pred.csv')
df_target = pd.read_csv('YOUR_PATH/output/df_target.csv')

X_train, X_test, y_train, y_test = train_test_split(df_train, df_target,
                                                    test_size=0.2, random_state=1)

# region filtered features by Lasso
filtered_features = [
'RoofMatl_WdShngl',
'Neighborhood_StoneBr',
'Neighborhood_NridgHt',
'Neighborhood_NoRidge',
'GrLivArea',
'SaleType_New',
'OverallQual',
'Neighborhood_Crawfor',
'Exterior1st_BrkFace',
'Exterior2nd_ImStucc',
'Condition1_Norm',
'LotConfig_CulDSac',
'BsmtFinSF1',
'YearBuilt',
'LandContour_HLS',
'GarageCars',
'Neighborhood_Somerst',
'OverallCond',
'TotRmsAbvGrd',
'Functional',
'LotArea',
'WoodDeckSF',
'TotalBsmtSF',
'HouseStyle_SFoyer',
'HouseStyle_SLvl',
'ScreenPorch',
'BsmtFullBath',
'Fireplaces',
'Neighborhood_BrkSide',
'FullBath',
'BsmtFinSF2',
'LandSlope',
'LotShape_IR2',
'HeatingQC_Ex',
'PoolArea',
'Exterior1st_MetalSd',
'Foundation_PConc',
'HalfBath',
'1stFlrSF',
'BsmtHalfBath',
'ExterCond',
'MiscVal',
'SaleType_WD',
'HeatingQC_Gd',
'Exterior1st_Wd Sdng',
'EnclosedPorch',
'3SsnPorch',
'Neighborhood_Gilbert',
'OpenPorchSF',
'Neighborhood_NAmes',
'YrSold',
'Exterior1st_HdBoard',
'Neighborhood_OldTown',
'HouseStyle_1.5Fin',
'RoofStyle_Gable',
'LowQualFinSF',
'MoSold',
'SaleType_COD',
'BldgType_Twnhs',
'KitchenAbvGr',
'BedroomAbvGr',
'HouseStyle_2Story',
'SaleCondition_Abnorml',
'Neighborhood_Edwards',
'MSSubClass',
'ExterQual',
'Neighborhood_NWAmes',
'BldgType_TwnhsE',
'KitchenQual',
'LandContour_Bnk',
'Condition2_PosN',
'RoofMatl_ClyTile'
]
# endregion ()

X_train = X_train[filtered_features]
X_test = X_test[filtered_features]

# L1 regularization (sparse)
model = RidgeCV(
            cv=10
            #alphas=
      )

model = model.fit(X_train, y_train)

y_pred_cv = model.predict(X_train)
y_pred_test = model.predict(X_test)

print('Best Alpha : ' + str(model.alpha_))

print('Train MAE : ' + str(mean_absolute_error(y_train, y_pred_cv)))
print('Train R^2 : ' + str(r2_score(y_train, y_pred_cv)))

print('Test R^2 : ' + str(r2_score(y_test, y_pred_test)))
print('Test MSE : ' + str(mean_squared_error(y_test, y_pred_test)))
print('Test MAE : ' + str(mean_absolute_error(y_test, y_pred_test)))

# export to excel
# pd.DataFrame(y_test).to_csv('YOUR_PATH/output/y_test_lasso.csv', index=False, header=['True Value'])
# pd.DataFrame(y_pred_test).to_csv('YOUR_PATH/output/y_pred_test_lasso.csv', index=False, header=['Pred'])

'''
Best Alpha : 10.0
Train MAE : 16955.7932398
Train R^2 : 0.876719148189
Test R^2 : 0.891734627496
Test MSE : 608150920.345
Test MAE : 18227.3300117
'''

'''
Feature Selection by Lasso 
Best Alpha : 1.0
Train MAE : 16714.9505943
Train R^2 : 0.89321932256
Test R^2 : 0.894231478281
Test MSE : 594125548.54
Test MAE : 17360.9212422
'''