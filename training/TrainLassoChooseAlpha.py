import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LassoCV
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.feature_selection import SelectFromModel

df_train = pd.read_csv('YOUR_PATH/output/df_train.csv')
df_pred = pd.read_csv('YOUR_PATH/output/df_pred.csv')
df_target = pd.read_csv('YOUR_PATH/output/df_target.csv')

X_train, X_test, y_train, y_test = train_test_split(df_train, df_target,
                                                    test_size=0.2, random_state=1)

# region filtered features by Lasso
filtered_features = [
'RoofMatl_WdShngl',
'Neighborhood_StoneBr',
'Neighborhood_NridgHt',
'Neighborhood_NoRidge',
'GrLivArea',
'SaleType_New',
'OverallQual',
'Neighborhood_Crawfor',
'Exterior1st_BrkFace',
'Exterior2nd_ImStucc',
'Condition1_Norm',
'LotConfig_CulDSac',
'BsmtFinSF1',
'YearBuilt',
'LandContour_HLS',
'GarageCars',
'Neighborhood_Somerst',
'OverallCond',
'TotRmsAbvGrd',
'Functional',
'LotArea',
'WoodDeckSF',
'TotalBsmtSF',
'HouseStyle_SFoyer',
'HouseStyle_SLvl',
'ScreenPorch',
'BsmtFullBath',
'Fireplaces',
'Neighborhood_BrkSide',
'FullBath',
'BsmtFinSF2',
'LandSlope',
'LotShape_IR2',
'HeatingQC_Ex',
'PoolArea',
'Exterior1st_MetalSd',
'Foundation_PConc',
'HalfBath',
'1stFlrSF',
'BsmtHalfBath',
'ExterCond',
'MiscVal',
'SaleType_WD',
'HeatingQC_Gd',
'Exterior1st_Wd Sdng',
'EnclosedPorch',
'3SsnPorch',
'Neighborhood_Gilbert',
'OpenPorchSF',
'Neighborhood_NAmes',
'YrSold',
'Exterior1st_HdBoard',
'Neighborhood_OldTown',
'HouseStyle_1.5Fin',
'RoofStyle_Gable',
'LowQualFinSF',
'MoSold',
'SaleType_COD',
'BldgType_Twnhs',
'KitchenAbvGr',
'BedroomAbvGr',
'HouseStyle_2Story',
'SaleCondition_Abnorml',
'Neighborhood_Edwards',
'MSSubClass',
'ExterQual',
'Neighborhood_NWAmes',
'BldgType_TwnhsE',
'KitchenQual',
'LandContour_Bnk',
'Condition2_PosN',
'RoofMatl_ClyTile'
]
# endregion ()

# region filtered NEW features by Lasso
new_filtered_features = [
'AllSF-3',
'AllFlrsSF-2',
'OverallQual-s3',
'AllSF',
'SaleType_New',
'Neighborhood_NoRidge',
'YearBuilt',
'BsmtFinSF1',
'Neighborhood_NridgHt',
'Neighborhood_StoneBr',
'Condition1_Norm',
'OverallCond',
'KitchenQual-Sq',
'GarageCars-3',
'LotArea',
'Neighborhood_Crawfor',
'OverallQual-Sq',
'ExterQual-Sq',
'MSSubClass',
'Functional',
'LotConfig_CulDSac',
'KitchenAbvGr',
'Exterior1st_BrkFace',
'TotalBath-3',
'Fireplaces',
'1stFlrSF',
'Foundation_CBlock',
'ScreenPorch',
'WoodDeckSF',
'BedroomAbvGr',
'SaleCondition_Abnorml',
'Foundation_PConc',
'MoSold',
'PoolArea',
'LandContour_Bnk',
'TotRmsAbvGrd',
'Neighborhood_NWAmes',
'BsmtFinSF2',
'TotalBsmtSF',
'Neighborhood_Edwards',
'LotShape_Reg',
'OpenPorchSF',
'ExterCond',
'KitchenQual-3',
'HeatingQC_Ex',
'MSZoning_RM',
'YrSold',
'LandSlope',
'GarageScore-2',
'GarageArea',
'CentralAir',
'GarageCars-Sq',
'LotConfig_Inside',
'YearRemodAdd'
]
# endregion

# X_train = X_train[new_filtered_features]
# X_test = X_test[new_filtered_features]

# L1 regularization (sparse) + otomatis grid search
model = LassoCV(
            cv=10,
            # alphas=[1, 10, 100, 150, 166, 200, 300], # 166 dpt dr pilihan otomatis, tanpa param alphas
            n_jobs=-1,
            random_state=1
      )

model = model.fit(X_train, y_train)

print('Best Alpha (Automatic): ' + str(model.alpha_))

alpha = model.alpha_

model = LassoCV(alphas = [alpha * .6, alpha * .65, alpha * .7, alpha * .75, alpha * .8,
                          alpha * .85, alpha * .9, alpha * .95, alpha, alpha * 1.05,
                          alpha * 1.1, alpha * 1.15, alpha * 1.25, alpha * 1.3, alpha * 1.35,
                          alpha * 1.4],
                # max_iter = 50000,
                cv = 10,
                n_jobs=-1,
                random_state=1)

model.fit(X_train, y_train)

print('Best Alpha (2nd Iteration): ' + str(model.alpha_))

y_pred_cv = model.predict(X_train)
y_pred_test = model.predict(X_test)

print('CV MAE : ' + str(mean_absolute_error(y_train, y_pred_cv)))
print('CV R^2 (Mean) : ' + str(r2_score(y_train, y_pred_cv)))

print('Test R^2 : ' + str(r2_score(y_test, y_pred_test)))
print('Test MSE : ' + str(mean_squared_error(y_test, y_pred_test)))
print('Test MAE : ' + str(mean_absolute_error(y_test, y_pred_test)))

# export to excel
pd.DataFrame(y_test).to_csv('YOUR_PATH/output/y_test_lasso.csv', index=False, header=['True Value'])
pd.DataFrame(y_pred_test).to_csv('YOUR_PATH/output/y_pred_test_lasso.csv', index=False, header=['Pred'])
pd.DataFrame(X_train.columns.values).to_csv('YOUR_PATH/output/features.csv', index=False, header=['Features'])
pd.DataFrame(model.coef_).to_csv('YOUR_PATH/output/lasso_coef.csv', index=False, header=['Coef'])
